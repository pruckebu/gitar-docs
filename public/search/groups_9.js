var searchData=
[
  ['mem',['Mem',['../da/d08/group__mem.html',1,'']]],
  ['memory_20block_20management_20functions',['Memory block management functions',['../db/df1/group__memb.html',1,'']]],
  ['managed_20memory_20allocator',['Managed memory allocator',['../d6/dd4/group__mmem.html',1,'']]],
  ['multi_2dthreading_20library',['Multi-threading library',['../d7/d1f/group__mt.html',1,'']]],
  ['mesh_20routing',['Mesh routing',['../d5/d8f/group__rimemesh.html',1,'']]],
  ['multi_2dhop_20reliable_20bulk_20data_20transfer',['Multi-hop reliable bulk data transfer',['../d1/dc2/group__rudolph1.html',1,'']]]
];
