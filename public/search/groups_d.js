var searchData=
[
  ['single_2dhop_20reliable_20unicast',['Single-hop reliable unicast',['../d9/db6/group__rimerunicast.html',1,'']]],
  ['stubborn_20best_2deffort_20local_20area_20broadcast',['Stubborn best-effort local area broadcast',['../dc/d9e/group__rimestbroadcast.html',1,'']]],
  ['stubborn_20unicast',['Stubborn unicast',['../d1/d63/group__rimestunicast.html',1,'']]],
  ['single_2dhop_20unicast',['Single-hop unicast',['../df/d16/group__rimeuc.html',1,'']]],
  ['single_2dhop_20reliable_20bulk_20data_20transfer',['Single-hop reliable bulk data transfer',['../d1/dc1/group__rudolph0.html',1,'']]],
  ['single_2dhop_20reliable_20bulk_20data_20transfer',['Single-hop reliable bulk data transfer',['../d8/df7/group__rudolph2.html',1,'']]],
  ['seconds_20timer_20library',['Seconds timer library',['../d9/da6/group__stimer.html',1,'']]],
  ['sys',['Sys',['../d8/d14/group__sys.html',1,'']]]
];
